#!/bin/sh
rootpath=$PWD

cd $rootpath/meteo_prev/meteo_prev_node/ && sudo docker build -t meteoprev .
cd $rootpath/web_app/ApplicationWeb_SSPC/back/ && sudo docker build -t backweb .
cd $rootpath/web_app/ApplicationWeb_SSPC/front/ && sudo docker build -t frontweb .
cd $rootpath/datasrc/docker/ && sudo docker build -t chauffage .
