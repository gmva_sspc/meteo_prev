module.exports = function(app) {
    let indexRouter = require('./routes/index');
    let communesRouter = require('./routes/communes');
    let historyRouter = require('./routes/getHistory');
    let settingsRouter = require('./routes/settings');
    let tasksRouter = require('./routes/tasks');

    app.use('/', indexRouter);
    app.use('/communes', communesRouter);
    app.use('/history', historyRouter);
    app.use('/settings', settingsRouter);
    app.use('/tasks', tasksRouter);
}