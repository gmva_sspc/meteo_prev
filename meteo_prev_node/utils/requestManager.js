const netatmo = require('../API/netatmo.js');
const openweathermap = require('../API/openweathermap.js');
const cron = require('node-cron');
const communes = require('../communes.js');
const db = require('../database/influxDB.js');

const fs = require('fs');


function OWMTempsReel(communesSelected, bucket) {
    communesSelected.forEach(commune => {
        openweathermap.getCurrWithCoords(commune.coords, async (result) => {
            await db.write(bucket, commune.name, openweathermap.parseForInfluxDB(result));
        }, (err) => {
            console.log(err);
        });
        console.log("Delay passed");
    });
}


async function TaskHistory(start, end, communesSelected, bucket) {
    start = parseInt(start/1000);
    end = parseInt(end/1000);
    while( start < end ) {
        await communesSelected.forEach(async commune => {
            openweathermap.getHistWithCoords(commune.coords, start, end, async (result) => {
                for(let i = 0; i < result.list.length; i++) {
                    printDate(result.list[i].dt);
                    // console.log("Commune : " + commune.name + " - date : " + new Date(result.list[i].dt*1000) + " - temp : " + openweathermap.parseHistForInfluxDB(result.list[i]).temperature)
                    await db.write(bucket, commune.name, openweathermap.parseHistForInfluxDB(result.list[i]), result.list[i].dt*1000);
                    await delay(1000);
                }
            }, (err) => {
                console.log(err);
            });
        });
        start += 604800;// 1 week = 604800 seconds
        await delay(170*1000);
    }
}

async function ForecastWeather() {
    console.log("Task OWM_forecast started");
    
    await openweathermap.getForecastWithCoords(communes.filter(commune => commune.name === "VANNES")[0].coords, (result) => {
        console.log(result);
    }, (err) => {
        console.log(err);
    });

}


let timestampAlreadyPast = [];
function printDate(timestamp) {
    if(timestampAlreadyPast.indexOf(timestamp) === -1) {
        timestampAlreadyPast.push(timestamp);
        console.log("Date : " + new Date(timestamp*1000) + " " + timestamp*1000);
    }
}

const delay = (waitTimeInMs) => new Promise(resolve => setTimeout(resolve, waitTimeInMs));

module.exports = {
    netatmo,
    openweathermap,
    TaskHistory,
    OWMTempsReel,
    ForecastWeather
}










