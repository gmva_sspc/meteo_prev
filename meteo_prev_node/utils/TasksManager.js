
const cron = require('node-cron');
const requestManager = require('./requestManager.js');

let Tasks = [];

class TasksManager {

    static createTask(name, numberTime, timeType, communes, bucket) {
        console.log('createTask');

        let cronTime = this.convertToCron(numberTime, timeType);
        if(cronTime === "Invalid time type") return "Invalid time type";
        let cronTask = cron.schedule(cronTime, () => {
            requestManager.OWMTempsReel(communes, bucket);
            console.log(`Task ${name} executed`);
        });

        Tasks.push({
            "name": name,
            "humanTime": `${(timeType == "day" || timeType == "month") ? "Tous les" : "Toutes les"} ${numberTime > 1 ? numberTime : ""} ${this.TimeTypeFrench(timeType)}`,
            "cronTime": cronTime,
            "communes": communes,
            "bucket": bucket,
            "task": cronTask
        });

        console.log(Tasks);

        return "Task created";
    }

    static removeTask(taskName) {
        Tasks.forEach((item, index) => {
            if(item.name === taskName) {
                item.task.stop();
                Tasks.splice(index, 1);
            }
        });
    }

    static getTasks() {
        return Tasks;
    }

    static convertToCron(numberTime, timeType) {
        switch (timeType) {
            case 'second':
                return `*/${numberTime} * * * * *`;
            case 'minute':
                return `*/${numberTime} * * * *`;
            case 'hour':
                return `* */${numberTime} * * *`;
            case 'day':
                return `* * */${numberTime} * *`;
            case 'month':
                return `* * * */${numberTime} *`;
            default:
                return "Invalid time type";
        }
    }

    static TimeTypeFrench (timeType) {
        switch (timeType) {
            case 'second':
                return 'secondes';
            case 'minute':
                return 'minutes';
            case 'hour':
                return 'heures';
            case 'day':
                return 'jours';
            case 'month':
                return 'mois';
            default:
                return "Invalid time type";
        }
    }

}

module.exports = TasksManager;