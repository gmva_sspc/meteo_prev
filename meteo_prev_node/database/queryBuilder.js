class queryBuilder {

    constructor() {
        this.bucket = "";
        this.range = "";
        this.filter = [];
        this.last = false;
        this.first = false;
    }

    setBucket(bucketName) {
        this.bucket = `from(bucket: "${bucketName}")`;
        return this;
    }

    setRange(start, end) {
        if(end == null || end == undefined) this.range = `range(start: ${start})`;
        else this.range = `range(start: ${start}, stop: ${end})`;
        return this;
    }

    setFilter(columnName, arrayFilter) {
        let filterStr = "filter(fn: (r) => ";

        if(arrayFilter.length <= 0) throw new Error("ArrayFilter is empty");

        filterStr += `r["${columnName}"] == "${arrayFilter[0]}"`;

        if(arrayFilter.length > 1) {
            for(let i = 1; i < arrayFilter.length; i++) {
                filterStr += ` or r["${columnName}"] == "${arrayFilter[i]}"`;
            }
        }
        filterStr += ")";
        this.filter.push(filterStr);
        return this;
    }

    setLast() {
        this.first = false;
        this.last = true;
        return this;
    }

    setFirst() {
        this.last = false;
        this.first = true;
        return this;
    }

    build() {
        let query = this.bucket + "\n";
        if(this.range != "") query += `  |> ${this.range}\n`;
        if(this.filter.length > 0) {
            for(let i = 0; i < this.filter.length; i++) {
                query += `  |> ${this.filter[i]}\n`;
            }
        }
        if(this.last) query += "  |> last()";
        if(this.first) query += "  |> first()";
        return query;
    }
}

module.exports = queryBuilder;