
const config = require("../jsons/config.json");
const {
    InfluxDB,
    Point
} = require('@influxdata/influxdb-client')

const url = config.database.host;
const org = config.database.org;
let connection = null;

function getConnection(token) {
    if (connection == null) {
        connection = new InfluxDB({
            url,
            token
        });
    }
    return connection;
}

function write(bucket, mesure, info, timestamp) {
    return new Promise( (res, rej) => {
        let writeClient = getConnection(bucket.token).getWriteApi(org, bucket.name, 'ns');

        let point = new Point(mesure);

        if(timestamp) point.timestamp(timestamp*1000000);
        
        for (const [key, value] of Object.entries(info)) {
            if(!isNaN(value)) {
                point.floatField(key, value);
            } else {
                point.stringField(key, value);
            }
        }
        console.log(point)
        console.log(point.toLineProtocol(writeClient))
        res(writeClient.writePoint(point));
    })
}


function read(org, token, query) {
    return new Promise((resolve, reject) => {
        let queryClient = getConnection(token).getQueryApi(org);
        let fluxQuery = query;
        let res = [];
        queryClient.queryRows(fluxQuery, {
            next: (row, tableMeta) => { // RESULTAT
                const tableObject = tableMeta.toObject(row)
                res.push(tableObject);
            },
            error: (error) => { // ERREUR
                console.error('\nError', error)
                reject(error);
            },
            complete: () => { // FINIS
                console.log('\nSuccess')
            resolve(res);
            },
        })
    });
}

module.exports = {
    getConnection,
    write,
    read
}