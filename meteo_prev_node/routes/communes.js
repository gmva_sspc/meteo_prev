const express = require('express');
const queryBuilder = require('../database/queryBuilder.js');
const db = require('../database/influxDB.js');
const config = require('../jsons/config.json');
const allCommunes = require('../communes.js');

let router = express.Router();

/* GET home page. */
router.get('/', async function(req, res, next) {
  
  let query = new queryBuilder().setBucket(config.communesBucket).setLast().setRange('-7d').build();

  let token = config.database.buckets.filter(bucket => bucket.name == config.communesBucket)[0].token;

  
  db.read(config.database.org, token, query).then(result => {

    allCommunes.forEach(commune => commune.data = {});

    result.forEach(data => {
      let commune = allCommunes.filter(commune => commune.name == data._measurement)[0];
      commune.data[data._field] = data._value;
    });

    
    // console.log(allCommunes.filter(commune => commune.name == 'VANNES')[0].data);
    // console.log("rendering");
    res.render('communes', {communes: allCommunes});
  });
});

module.exports = router;
