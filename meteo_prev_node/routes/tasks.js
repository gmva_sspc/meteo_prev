const express = require('express');
const TasksManager = require('../utils/TasksManager.js');
const allCommunes = require('../communes.js');
const settings = require('../jsons/config.json');

let router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
    res.render('tasks', { tasks: TasksManager.getTasks(), communes: allCommunes, settings });
}); 

router.get('/delete/:taskname', function(req, res, next) {
    console.log(req.params.taskname);
    TasksManager.removeTask(req.params.taskname);
    res.redirect('/tasks');
}); 

router.post('/create', function(req, res, next) {
    /** @type Array */
    let communesList = req.body.communes.toString().split(",");

    if(req.body.taskname === "" || req.body.numberTime === "" || req.body.timeType === "" || req.body.numberTime < 1 || req.body.numberTime > 59) {
        res.redirect('/tasks');
        return;
    }

    if(req.body.communes === undefined) {
        res.redirect('/tasks');
        return;
    }

    console.log("Communes : " + JSON.stringify(communesList));
    if(communesList.includes("All")) {
        communesList = allCommunes;
    } else {
        communesList = allCommunes.filter(commune => commune.name == communesList[communesList.indexOf(commune.name)]);
    }

    console.log("Communes : " + JSON.stringify(communesList));

    let bucket = settings.database.buckets.filter(bucket => bucket.name == req.body.bucket)[0];

    TasksManager.createTask(req.body.taskname, req.body.numberTime, req.body.timeType, communesList, bucket);
    res.redirect('/tasks');
});

module.exports = router;
