const express = require('express');
const { TaskHistory } = require('../utils/requestManager');
const config = require('../jsons/config.json');
const allCommunes = require('../communes.js');

let router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('getHistory', { communes: allCommunes, settings: config });
});

router.post('/', function(req, res, next) {
    let start = req.body.datedeb.split(' ');
    let end = req.body.datefin.split(' ');
    let heuredeb = req.body.heuredeb;
    let heurefin = req.body.heurefin;
    
    let bucket = config.database.buckets.filter(bucket => bucket.name == req.body.bucket)[0];
    /** @type Array */
    let communesList = req.body.communes.toString().split(",");

    
    if(start.length != 3) return res.redirect('/');
    if(end.length != 3) return res.redirect('/');
    
    if(communesList.includes("All")) {
        communesList = allCommunes;
    } else {
        communesList = allCommunes.filter(commune => commune.name == communesList[communesList.indexOf(commune.name)]);
    }
    
    let datestart = new Date(`${start[1]} ${start[0]}, ${start[2]} ${heuredeb}:00`);
    let dateend = new Date(`${end[1]} ${end[0]}, ${end[2]} ${heurefin}:00`);

    if(dateend.getTime() > new Date().getTime()) return res.redirect('/');

    const oneYear = new Date().getTime()-31556926000;
    if(datestart.getTime() < oneYear || dateend.getTime() < oneYear) return res.redirect('/');

    if(datestart.getTime() >= dateend.getTime()) return res.redirect('/');

    TaskHistory(datestart.getTime(), dateend.getTime(), communesList, bucket);
    
    console.log(datestart.getTime());
    console.log(dateend.getTime());
    res.redirect('/');
});


module.exports = router;