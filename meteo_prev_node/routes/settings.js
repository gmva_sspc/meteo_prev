const express = require('express');
const config = require('../jsons/config.json');
const fs = require('fs');

let router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  console.log(config);
  res.render('settings', config);
}); 

router.post('/', function(req, res, next) {
  console.log(req.body);
  let newConfig = {
    "port": req.body.port,
    "communesBucket": req.body.bucketCommunes,
    "tokens": {
      "openweathermap": req.body.OWMToken,
      "netatmo": req.body.NetatmoToken,
      "netatmoRefresh": req.body.NetatmoRefreshToken
    },
    "database": {
      "host": req.body.host,
      "org": req.body.organization,
      "buckets": []
    }
  }

  for(let i = 0; i < req.body.nbBuckets; i++) {
    let bucket = req.body[`bucket-${i}`];
    newConfig.database.buckets.push({
      "name": bucket[0],
      "token": bucket[1]
    })
  }
  
  fs.writeFileSync('./jsons/config.json', JSON.stringify(newConfig, null, 2));
  res.redirect('/');
});

module.exports = router;
