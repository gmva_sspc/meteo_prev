const express = require('express');
const path = require('path');
const logger = require('morgan');
const db = require('./database/influxDB.js');
const app = express();

const config = require('./jsons/config.json');

const port = config.port || 3000;

app.disable('x-powered-by');
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));

// Routes :
require("./defineRoute.js")(app);

app.get("/OWM/:city", (req, res) => {
    console.log("TODO");
    // TODO : Request DB for the weather of the city.
});

// TODO : WebPage with the weather of the day of each city.


app.listen(port, () => {
    console.log(`Server started on port ${port}`);
});