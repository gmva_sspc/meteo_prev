const axios = require('axios');
const communes = require('../communes.js');
const config = require('../jsons/config.json');

// This file is used to call OpenWeatherMap Api.

let OWM_realtime = "https://api.openweathermap.org/data/2.5/weather?lat&lon&appid";
let OWM_history = "https://history.openweathermap.org/data/2.5/history/city?lat&lon&appid&start&end";
let OWM_forecast = "https://api.openweathermap.org/data/2.5/forecast?lat&lon&appid";


const token = config.tokens.openweathermap;

let getCurrWithCoords = async (coords, res, err) => {
    getCurrentWeather(coords, res, err);
}

let getCurrWithName = async (name, res, err) => {
    let commune = communes.find(commune => commune.name === name);
    getCurrentWeather(commune.coords, res, err);
}

let getHistWithCoords = async (coords, start, end, res, err) => {
    getHistoryWeather(coords, start, end, res, err);
}

/**
 * Get Weather with coords
 * @param {Array<Number>} coords The coords of the place
 * @param {*} res response function
 * @param {*} err error function
 */
async function getCurrentWeather(coords, res, err) {
    let url = OWM_realtime.replace("lat", "lat=" + coords[0]).replace("lon","lon=" + coords[1]).replace("appid", "appid=" + token);
    let result = await axios({
        method: 'get',
        url: url,
        accept: 'application/json',
        responseType: 'json',
    }).then(res => {
        return (res);
    }).catch(err => {
        return (err);
    });
    if(result.status != 200) err(result);
    else res(result.data);
}

/**
 * Get Weather with coords
 * @param {Array<Number>} coords The coords of the place
 * @param {Number} start
 * @param {Number} end
 * @param {*} res response function
 * @param {*} err error function
 */
async function getHistoryWeather(coords, start, end, res, err) {
    let url = OWM_history.replace("lat", "lat=" + coords[0]).replace("lon","lon=" + coords[1]).replace("appid", "appid=" + token).replace("start", "start=" + start).replace("end", "end=" + end)
    let result = await axios({
        method: 'get',
        url: url,
        accept: 'application/json',
        responseType: 'json',
    }).then(res => {
        return (res);
    }).catch(err => {
        return (err);
    });
    if(result.status != 200) err(result);
    else res(result.data);
}


async function getForecastWithCoords(coords, res, err) {
    getForecast(coords, res, err);
}

async function getForecastWithName(name, res, err) {
    let commune = communes.find(commune => commune.name === name);
    getForecast(commune.coords, res, err);
}

async function getForecast(coords, res, err) {
    let url = OWM_forecast.replace("lat", "lat=" + coords[0]).replace("lon","lon=" + coords[1]).replace("appid", "appid=" + token);
    let result = await axios({
        method: 'get',
        url: url,
        accept: 'application/json',
        responseType: 'json',
    }).then(res => {
        return (res);
    }).catch(err => {
        return (err);
    });
    if(result.status != 200) err(result);
    else res(result.data);
}


function parseForInfluxDB(data) {
    let result = {
        temperature: (data.main.temp-273.15).toFixed(2),
        temperatureMin: (data.main.temp_min-273.15).toFixed(2),
        temperatureMax: (data.main.temp_max-273.15).toFixed(2),
        feelsLike: data.main.feels_like,
        humidity: data.main.humidity,
        pressure: data.main.pressure,
        windSpeed: data.wind.speed,
        windDeg: data.wind.deg,
        clouds: data.clouds.all,
        rain: data.rain ? data.rain["1h"] : 0,
        snow: data.snow ? data.snow["1h"] : 0,
        weather: data.weather[0].main,
        weatherDesc: data.weather[0].description,
        weatherIcon: data.weather[0].icon,
        sunrise: data.sys.sunrise ? data.sys.sunrise : null,
        sunset: data.sys.sunset ? data.sys.sunset : null,
        country: data.sys.country ? data.sys.country: null,
        latitude: data.coord.lat ? data.coord.lat : null,
        longitude: data.coord.lon ? data.coord.lon : null
    }
    return (result);
}

function parseHistForInfluxDB(data) {
    let result = {
        temperature: (data.main.temp-273.15).toFixed(2),
        temperatureMin: (data.main.temp_min-273.15).toFixed(2),
        temperatureMax: (data.main.temp_max-273.15).toFixed(2),
        feelsLike: (data.main.feels_like-273.15).toFixed(2),
        humidity: data.main.humidity,
        pressure: data.main.pressure,
        windSpeed: data.wind.speed,
        windDeg: data.wind.deg,
        clouds: data.clouds.all,
        weather: data.weather[0].main,
        weatherDesc: data.weather[0].description,
        weatherIcon: data.weather[0].icon
    }
    return (result);
}

module.exports = {
    getCurrentWeather,
    getCurrWithCoords,
    getCurrWithName,
    getHistWithCoords,
    getForecastWithCoords,
    getForecastWithName,
    parseForInfluxDB,
    parseHistForInfluxDB
}
