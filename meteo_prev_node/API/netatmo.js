const axios = require('axios');
const config = require('../jsons/config.json');

// This file is used to call Netatmo Api.

let netatmoUrl = "https://api.netatmo.com/api/getpublicdata?lat_ne=lattr&lon_ne=lontr&lat_sw=latbl&lon_sw=lonbl&required_data=temperature&filter=true";


const token = config.tokens.netatmo;
const token2 = config.tokens.netatmoRefresh; // TODO : Refresh the token regularly.

/**
 * Get Measures of stations in a zone
 * @param {Array<Number>} NorthE Array of coordinates of the North-East corner of the zone
 * @param {Array<Number>} SouthW Array of coordinates of the South-West corner of the zone
 * @param {*} res response function
 * @param {*} err error function
 */
let getWeather = async (NorthE, SouthW, res, err) => {
    let url = netatmoUrl.replace("lattr", NorthE[0]).replace("lontr", NorthE[1]).replace("latbl", SouthW[0]).replace("lonbl", SouthW[1]);
    let result = await axios({
        method: 'get',
        url: url,
        accept: 'application/json',
        responseType: 'json',
        headers: {
            "Authorization": "Bearer " + token
        }
    }).then(res => {
        return (res.data);
    }).catch(err => {
        return (err);
    });
    if(result.status != 200) err(result);
    else res(result);
}

module.exports = {
    getWeather
}
