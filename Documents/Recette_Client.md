# Recette Client

|                          Tâche                          |       Statut       |
|:-------------------------------------------------------:|:------------------:|
|          Récupération de données en temps réel          | :heavy_check_mark: |
| Récupération de données sur une période de temps passée | :heavy_check_mark: |
|          Récupération de données prévisionnels          | :heavy_check_mark: |
|                 Traitement des données                  | :heavy_check_mark: |
|           Stockage des données dans InfluxDb            | :heavy_check_mark: |
|   Mettre à disposition les données aux autres groupes   | :heavy_check_mark: |
|   Interface pour faciliter le paramètrage des scripts   | :heavy_check_mark: |